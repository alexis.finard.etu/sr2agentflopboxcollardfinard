package serveurFtp;

import urlFlopBox.UrlFlopBox;
/**
 * Classe représentant un serveur ftp
 * @author collard finard
 *
 */
public class ServeurFtp {
	/**
	 * le nom du serveur ftp
	 */
	private String nomServeurFtp;
	/**
	 * le numéro de port associé 
	 */
	private String port;
	/**
	 * l'identifiant de l'utilisateur
	 */
	private String identifiant;
	/**
	 * le mot de passe de l'utilisateur
	 */
	private String pass;

	/**
	 * constructeur
	 * @param nomServeurFtp le nom du serveur ftp
	 * @param port le numéro de port
	 * @param identifiant l'indentifiant de l'utilisateur
	 * @param pass le mot de pass de l'utilisateur
	 */
	public ServeurFtp(String nomServeurFtp, String port, String identifiant, String pass) {
		this.nomServeurFtp = nomServeurFtp;
		this.port = port;
		this.identifiant = identifiant;
		this.pass = pass;
		UrlFlopBox.addServeur(this.nomServeurFtp);
	}
	/**
	 * renvoie le nom du serveur ftp
	 * @return le nom du serveur ftp
	 */
	public String getNomServeurFtp() { return this.nomServeurFtp; }
	/**
	 * renvoie le numéro de port
	 * @return le numéro de port
	 */
	public String getPort() { return this.port; }
	/**
	 * renvoie l'identifiant
	 * @return l'identifiant
	 */
	public String getIdentifiant() { 
		return this.identifiant; 
	}
	/**
	 * renvoie le mot de passe
	 * @return le mot de passe
	 */
	public String getPass() { return this.pass; }
}
