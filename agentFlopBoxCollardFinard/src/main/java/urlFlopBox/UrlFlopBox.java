package urlFlopBox;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

import sr2.App;

/**
 * Class permetant de gérer la connexion entre la machine est le serveur FlopBox
 * @author Collard, Finard
 */
public class UrlFlopBox {

	private static final int BUFFER_SIZE = 4096;

	/**
	 * Ajoute un serveur sur la plateforme flopbox
	 * @param nomServeurFtp String : nom du serveur
	 * @return String : code de retour
	 */
	public static String addServeur(String nomServeurFtp) {
		return sendUrl(nomServeurFtp, "", "POST", null);
	}

	/**
	 * Ajoute un alias sur la plateforme flopbox
	 * @param nomServeurFtp String : nom du serveur
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	public static String addAlias(String nomServeurFtp, Map<String, String> header) {
		return sendUrl(nomServeurFtp, "", "PUT", header);
	}

	/**
	 * Retourne le nom d'un serveur à partir de son alias
	 * @param nomAlias String : nom de l'alias
	 * @return String : code de retour et nom du serveur
	 */
	public static String getNomServeur(String nomAlias) {
		return sendUrl(nomAlias, "", "GET", null);
	}

	/**
	 * Renvoie la liste des serveurs sur flopbox
	 * @return String : code de retour et list des noms de serveur
	 */
	public static String getListNameServeur() {
		return sendUrl("", "", "GET", null);
	}

	/**
	 * liste le contenue d'un repertoire
	 * @param nomServeurFtp String : nom du serveur
	 * @param repertoire String : repertoire à lister
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour et listes des fichiers contenus dans le repertoire
	 */
	public static String getList(String nomServeurFtp, String repertoire, Map<String, String> header) {
		return sendUrl(nomServeurFtp, repertoire, "GET", header);
	}
	
	/**
	 * creer un nouveau repertoire
	 * @param nomServeurFtp String : nom du serveur
	 * @param repertoire String : repertoire à lister
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	public static String mkdir(String nomServeurFtp, String repertoire, Map<String, String> header) {
		return sendUrl(nomServeurFtp, "/directory"+repertoire, "POST", header);
	}

	/**
	 * renvoie la date de derniere modification d'un fichier
	 * @param nomServeurFtp String : nom du serveur
	 * @param cheminFichier String : chemin pour accéder au fichier
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour et valeur de la date
	 */
	public static String getLastModifFile(String nomServeurFtp, String cheminFichier, Map<String, String> header) {
		return sendUrl(nomServeurFtp, "/file/modif"+cheminFichier , "GET", header);
	}

	/**
	 * modifie la date de derniere modification d'un fichier
	 * @param nomServeurFtp String : nom du serveur
	 * @param cheminFichier String : chemin pour accéder au fichier
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	public static String setLastModifFile(String nomServeurFtp, String cheminFichier, Map<String, String> header) {
		return sendUrl(nomServeurFtp, "/file/modif"+cheminFichier , "POST", header);
	}

	/**
	 * Telecharge un fichier
	 * @param nomServeurFtp String : nom du serveur
	 * @param cheminFichier String : chemin pour accéder au fichier
	 * @param cheminRepertoire String : chemin pour accéder au dossier du fichier sur le serveur
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	public static String downloadFile(String nomServeurFtp, String cheminFichier, String cheminRepertoire, Map<String, String> header) {
		return sendUrlDownload(nomServeurFtp, "/file"+cheminFichier, cheminRepertoire+cheminFichier , "GET", header);
	}

	/**
	 * Upload un fichier
	 * @param nomServeurFtp String : nom du serveur
	 * @param cheminFichier String : chemin pour accéder au fichier
	 * @param cheminRepertoire String : chemin pour accéder au dossier du fichier sur le serveur
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	public static String uploadFile(String nomServeurFtp, String cheminFichier, String cheminRepertoire, Map<String, String> header) {
		return sendUrlUpload(nomServeurFtp, "/file"+cheminFichier, cheminRepertoire+cheminFichier , header);
	}

	/**
	 * Genere et envoi l'url pour telecharger un fichier
	 * @param nomServeurFtp String : nom du serveur
	 * @param action String : action et chemin à la fin de l'url
	 * @param fileName String : nom du fichier
	 * @param typeMsg String : type d'envoi d'url
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	private static String sendUrlDownload(String nomServeurFtp, String action, String fileName, String typeMsg, Map<String, String> header) {
		URL url = null;
		try {
			url = new URL(App.getAdresseFlopBox()+nomServeurFtp+action);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		try {
			HttpURLConnection urlCon;
			urlCon = (HttpURLConnection) url.openConnection();


			if(header!=null) {
				for (Map.Entry<String, String> param : header.entrySet()) { /* parametre header de la requete */
					urlCon.setRequestProperty(param.getKey(), param.getValue());
				}
			}

			urlCon.setRequestMethod(typeMsg); /* GET POST ... */

			InputStream inputStream = urlCon.getInputStream();

			creationDossierPrecedent(fileName);
			FileOutputStream outputStream = new FileOutputStream(fileName);
			int bytesRead = -1;
			byte[] buffer = new byte[BUFFER_SIZE];
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inputStream.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "500";
		}

		return "200";
	}

	/**
	 * Genere et envoi l'url pour l'upload d'un fichier
	 * @param nomServeurFtp String : nom du serveur
	 * @param action String : action et chemin à la fin de l'url
	 * @param fileName String : nom du fichier
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	private static String sendUrlUpload(String nomServeurFtp, String action, String fileName, Map<String, String> header) {
		
		PostMethod post = new PostMethod(App.getAdresseFlopBox()+nomServeurFtp+action);
		
		if(header!=null) {
			for (Map.Entry<String, String> param : header.entrySet()) { /* parametre header de la requete */
				post.setRequestHeader(param.getKey(), param.getValue());
			}
		}
		
		try {
			FilePart fp = new FilePart("file", new File(fileName));
			Part[] parts = { fp };
			post.setRequestEntity(new MultipartRequestEntity(parts, post.getParams()));
			return Integer.toString((new HttpClient().executeMethod(post)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "500";

	}

	/**
	 * Creer les dossiers pour la creation d'un fichier
	 * @param chemin String : chemin du fichier
	 */
	private static void creationDossierPrecedent(String chemin) {
		String[] cheminSplit = chemin.split("/");
		String cheminTmp = "";
		for(int i=0;i<cheminSplit.length-1;i++) {
			cheminTmp += cheminSplit[i]+"/";
			File f = new File(cheminTmp);
			if(!f.exists()) {
				f.mkdir();					
			}
		}
	}

	/**
	 * 
	 * @param nomServeurFtp String : nom du serveur
	 * @param action String : action et chemin à la fin de l'url
	 * @param typeMsg String : type d'envoi d'url
	 * @param header Map<String,String> : map contenant les différentes information à transmettre à la plateforme flopbox
	 * @return String : code de retour
	 */
	private static String sendUrl(String nomServeurFtp, String action, String typeMsg, Map<String, String> header) {
		URL url = null;
		String valRet = "";

		try {
			url = new URL(App.getAdresseFlopBox()+nomServeurFtp+action);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		try {
			HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();

			if(header!=null) {
				for (Map.Entry<String, String> param : header.entrySet()) { /* parametre header de la requete */
					urlCon.setRequestProperty(param.getKey(), param.getValue());
				}
			}

			urlCon.setRequestMethod(typeMsg); /* GET POST ... */

			int responseCode = urlCon.getResponseCode();
			valRet += String.valueOf(responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				valRet += "\n"+inputLine;
			}
			in.close();

			return valRet;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error";
	}
}
