package fichier;

import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import exception.FTPServeurException;
import serveurFtp.ServeurFtp;
import urlFlopBox.UrlFlopBox;

/**
 * Classe permetant de gerer les fichiers distants
 * @author Collard, Finard
 */
public class FichierDistant extends Fichier {
	
	private ServeurFtp serveurFtp;

	/**
	 * Constructeur d'un fichier a partir du nom et du chemin de celui ci
	 * @param nom String : nom du fichier
	 * @param chemin String : chemin du fichier
	 * @param serveurFtp ServeurFtp : serveur ou se trouve le fichier distant
	 */
	public FichierDistant(String nom, String chemin, ServeurFtp serveurFtp) {
		super(nom, chemin);
		this.serveurFtp = serveurFtp;
	}

	/**
	 * Retourne la date de derniere modification au format FileTime
	 * @return FileTime : Date de derniere modification du fichier
	 * @throws FTPServeurException : Execption en cas d'echec
	 */
	public FileTime getDateDerniereModification() throws FTPServeurException {
			
		Map<String, String> map = new HashMap<String, String>();
		map.put("username", this.serveurFtp.getIdentifiant());
		map.put("password", this.serveurFtp.getPass());
		map.put("port", this.serveurFtp.getPort());
		
		String date = UrlFlopBox.getLastModifFile(this.serveurFtp.getNomServeurFtp(), cheminFichier+nomFichier, map).split("\n")[1];
		String myDate = date.substring(0, 4)+"/"+date.substring(4, 6)+"/"+date.substring(6, 8)+" "+
						date.substring(8, 10)+":"+date.substring(10, 12)+":"+date.substring(12, 14);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		
    	try {
			return FileTime.fromMillis(sdf.parse(myDate).getTime());
		} catch (ParseException e) {
			throw new FTPServeurException("");
		} 
	}

	/**
	 * modifie la date de derniere modification du fichier
	 * @param time FileTime : Date de dernier modifaction a appliquer
	 * @throws FTPServeurException : Execption en cas d'echec
	 */
	public void setDateDerniereModification(FileTime time) throws FTPServeurException {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("username", this.serveurFtp.getIdentifiant());
		map.put("password", this.serveurFtp.getPass());
		map.put("port", this.serveurFtp.getPort());
		map.put("date", df.format(time.toMillis()));
		UrlFlopBox.getLastModifFile(this.serveurFtp.getNomServeurFtp(), cheminFichier+nomFichier, map);
	}

}