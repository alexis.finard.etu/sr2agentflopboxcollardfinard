package fichier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;

import exception.FTPServeurException;

/**
 * Classe permetant de gerer les fichiers locaux
 * @author Collard, Finard
 */
public class FichierLocal extends Fichier {

	/**
	 * Constructeur d'un fichier a partir du nom et du chemin de celui ci
	 * @param nom String : nom du fichier
	 * @param chemin String : chemin du fichier
	 */
	public FichierLocal(String nom, String chemin) {
		super(nom, chemin);
	}

	/**
	 * Retourne la date de derniere modification au format FileTime
	 * @return FileTime : Date de derniere modification du fichier
	 * @throws FTPServeurException : Execption en cas d'echec
	 */
	public FileTime getDateDerniereModification() {
		try {
			Path tempFile = Paths.get(this.cheminFichier+this.nomFichier);
	        return Files.getLastModifiedTime(tempFile);
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * modifie la date de derniere modification du fichier
	 * @param time FileTime : Date de dernier modifaction a appliquer
	 * @throws FTPServeurException : Execption en cas d'echec
	 */
	public void setDateDerniereModification(FileTime time) throws FTPServeurException {
		Path tempFile = Paths.get(this.cheminFichier+this.nomFichier);
		try {
			Files.setLastModifiedTime(tempFile, time);
		} catch (IOException e) {
			System.out.print("error = "+e+"\n");
			throw new FTPServeurException("Impossible de modifier la date du fichier");
		}
	}
}
