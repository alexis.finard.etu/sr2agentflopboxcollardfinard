package fichier;

import java.nio.file.attribute.FileTime;

import exception.FTPServeurException;

/**
 * Classe permetant de gerer les fichiers
 * @author Collard, Finard
 */
public abstract class Fichier {
	
	protected String nomFichier;
	protected String cheminFichier;
	
	/**
	 * Constructeur d'un fichier a partir du nom et du chemin de celui ci
	 * @param nom String : nom du fichier
	 * @param chemin String : chemin du fichier
	 */
	public Fichier(String nom, String chemin) {
		this.nomFichier = nom;
		this.cheminFichier = chemin;
	}
	
	/**
	 * Retourne la date de derniere modification au format FileTime
	 * @return FileTime : Date de derniere modification du fichier
	 * @throws FTPServeurException : Execption en cas d'echec
	 */
	public abstract FileTime getDateDerniereModification() throws FTPServeurException;
	
	/**
	 * modifie la date de derniere modification du fichier
	 * @param time FileTime : Date de dernier modifaction a appliquer
	 * @throws FTPServeurException : Execption en cas d'echec
	 */
	public abstract void setDateDerniereModification(FileTime time) throws FTPServeurException;

	/**
	 * 
	 * @param otherFile Fichier
	 * @return 0 Si les 2 fichiers ont été modifier en même temps, moins de 0 si other à été modifier plus ressament, plus de 0 sinon.
	 * @throws FTPServeurException 
	 */
	public int compDateModif(Fichier otherFile) throws FTPServeurException {
		return this.getDateDerniereModification().compareTo(otherFile.getDateDerniereModification());
	}
}
