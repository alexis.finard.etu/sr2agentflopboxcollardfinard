package sr2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import passerelleServeurLocal.PasserelleServeurLocal;
import serveurFtp.ServeurFtp;
import urlFlopBox.UrlFlopBox;


public class App 
{
	public static void main( String[] args )
	{
		// recupération nom serveur
		Map<String,String> portServeur = new HashMap<String, String>();
		Map<String,String> loginServeur = new HashMap<String, String>();
		Map<String,String> passServeur = new HashMap<String, String>();
		List<String> lines;
		try { // récupération des lignes du fichies
			lines = Files.readAllLines(Paths.get("LogFile.txt"));
		} catch (IOException e1) {
			System.out.println("Error : Can't read the log file");
			return;
		}

		if (lines.size() % 4 != 0) { // assertion
			System.out.println("Error : The log file do not have the expected size");
			return;
		}
		int i = 0;
		while (i < lines.size()) {
			String servername = lines.get(i++);
			loginServeur.put(servername, lines.get(i++));
			passServeur.put(servername, lines.get(i++));
			portServeur.put(servername, lines.get(i++));
		}

		File directoryFlopBox = new File("./serveurFTPflopbox/");
		if(!directoryFlopBox.exists()) { directoryFlopBox.mkdir(); }

		if(UrlFlopBox.getListNameServeur().split("\n").length>1){
			String[] tabNomServeur = UrlFlopBox.getListNameServeur().split("\n")[1].split(" ");
			List<ServeurFtp> listServeur = new ArrayList<ServeurFtp>(); 
			List<PasserelleServeurLocal> listPasserelle = new ArrayList<PasserelleServeurLocal>();

			for(String nomServeur : tabNomServeur) {
				if(portServeur.containsKey(nomServeur) && loginServeur.containsKey(nomServeur) && passServeur.containsKey(nomServeur)) {
					ServeurFtp s = new ServeurFtp(nomServeur, portServeur.get(nomServeur), loginServeur.get(nomServeur), passServeur.get(nomServeur));
					listServeur.add(s);    			
					listPasserelle.add(new PasserelleServeurLocal(s, "./serveurFTPflopbox/"+nomServeur+"/"));
				}
				else {
					System.out.print("Information connection au serveur "+nomServeur+" inconnu\n");
				}
			}


			while(true) {
				System.out.print("En cour de fonctionnement\n"+listPasserelle.size()+" serveurFTP gérer\n");
				for(PasserelleServeurLocal p : listPasserelle) {
					p.check();
				}


				try {
					TimeUnit.MINUTES.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else {
			System.out.print("Aucun serveurFTP gérer par flopbox");
		}

	}

	public static String getAdresseFlopBox() {
		return "http://localhost:8080/flopbox/";
	}
}
