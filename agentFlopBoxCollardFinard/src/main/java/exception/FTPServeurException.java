package exception;

/**
 * Classe représentant les exceptions liés aux serveurs FTP
 * @author collard finard
 *
 */
public class FTPServeurException extends Exception {

	/**
	 * L'excepetion
	 * @param msg le message de l'exception
	 */
	public FTPServeurException(String msg) {
		super(msg);
	}
}