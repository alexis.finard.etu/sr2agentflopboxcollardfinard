package passerelleServeurLocal;

import java.util.Map;
import java.util.TimeZone;

import exception.FTPServeurException;

import java.io.File;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import fichier.*;
import serveurFtp.ServeurFtp;
import urlFlopBox.UrlFlopBox;

/**
 * Classe permettant de faire la passerelle entre le serveur ftp et le repertoire local
 * @author Collard, Finard
 */
public class PasserelleServeurLocal {

	/**
	 * le serveur distant
	 */
	private ServeurFtp serveurFtp;
	/**
	 * le dossier local
	 */
	private String dossierLocal;
	/**
	 * la map des fichiers locaux
	 */
	private Map<String, FichierLocal> mapFichierLocal;
	/**
	 * la map des fichiers distants
	 */
	private Map<String, FichierDistant> mapFichierDistant;

	/**
	 * Constructeur de la passerelle
	 * @param serveurFtp le nom du serveur
	 * @param dossierLocal le nom du dossier
	 */
	public PasserelleServeurLocal(ServeurFtp serveurFtp, String dossierLocal) {
		this.serveurFtp = serveurFtp;
		this.dossierLocal = dossierLocal;
		this.mapFichierLocal= new HashMap<String, FichierLocal>();
		this.mapFichierDistant = new HashMap<String, FichierDistant>();
	}

	/**
	 * complète la map des fichiers locaux
	 * @param nomDossierReel le nom réel du répertoire
	 * @param nomDossierMap le nom du répertoire dans la map
	 */
	private void completeMapFichierLocal(String nomDossierReel, String nomDossierMap) {
		File repertoire = new File(nomDossierReel);
		if(!repertoire.exists()) {
			repertoire.mkdir();
		}
		String[] liste = repertoire.list();
		for (String nomFichier : liste) {
			File f = new File(nomDossierReel+nomFichier);
			if(f.isDirectory()) {
				completeMapFichierLocal(nomDossierReel+nomFichier+"/", nomDossierMap+nomFichier+"/");
			}
			else {
				if(this.mapFichierLocal.containsKey(nomDossierMap+nomFichier)) {
					this.mapFichierLocal.remove(nomDossierMap+nomFichier);
				}
				this.mapFichierLocal.put((nomDossierMap+nomFichier), new FichierLocal(nomFichier,nomDossierReel));
			}
		}
	}

	/**
	 * complète la map des fichiers distant
	 * @param nomDossier le dossier à ajouter
	 */
	private void completeMapFichierDistant(String nomDossier) {
		// Recuperation des fichiers dans le dossier nomDossier
		Map<String, String> header = new HashMap<String, String>();
		header.put("username",this.serveurFtp.getIdentifiant());
		header.put("password",this.serveurFtp.getPass());
		header.put("port",this.serveurFtp.getPort());
		String listFichier = UrlFlopBox.getList(this.serveurFtp.getNomServeurFtp(), nomDossier, header);	

		String[] tabFichier = listFichier.split("\n");
		for(int i=1; i<tabFichier.length;i++) {
			String nomFichier = getNameFile(tabFichier[i]);
			if(isDirectory(tabFichier[i])) {
				completeMapFichierDistant(nomDossier+nomFichier+"/");
			}
			else {
				if(this.mapFichierDistant.containsKey(nomDossier+nomFichier)) {
					this.mapFichierDistant.remove((nomDossier+nomFichier));
				}
				this.mapFichierDistant.put((nomDossier+nomFichier), new FichierDistant(nomFichier, nomDossier, this.serveurFtp));
			}
		}
	}

	/**
	 * renvoie le nom du fichier en fonction de la ligne
	 * @param ligneFichier la ligne du fichier
	 * @return le nom du fichier
	 */
	private String getNameFile(String ligneFichier) {
		String[] dataLigneFichier = ligneFichier.split(" ");
		return dataLigneFichier[dataLigneFichier.length-1];
	}

	/**
	 * renvoie vrai si la ligne fait référence à un répertoire
	 * @param ligneFichier la ligne du fichier
	 * @return vrai si c'est un répertoire
	 */
	private boolean isDirectory(String ligneFichier) {
		return ligneFichier.charAt(0)=='d';
	}

	/**
	 * synchronize le repertoire local et distant
	 */
	public void check() {
		//MAJ list Fichier local et distant
		completeMapFichierLocal(this.dossierLocal,"/");
		completeMapFichierDistant("/");
		for(String nomFichierLocal : this.mapFichierLocal.keySet()) {
			if(this.mapFichierDistant.containsKey(nomFichierLocal)) {
				try {
					int valComp = this.mapFichierLocal.get(nomFichierLocal).compDateModif(this.mapFichierDistant.get(nomFichierLocal));
					if(valComp<0) {
						download(nomFichierLocal);

					}
					else if(valComp>0) {
						upload(nomFichierLocal);
					}
				} catch (FTPServeurException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				upload(nomFichierLocal);
			}
		}
		for(String nomFichierDistant : this.mapFichierDistant.keySet()) {
			if(!this.mapFichierLocal.containsKey(nomFichierDistant)) {
				download(nomFichierDistant);
			}
		}
	}

	/**
	 * Télecharge un fichier distant 
	 * @param file le fichier à telecharger
	 */
	private void download(String file) {
		Map<String, String> header = new HashMap<String, String>();
		header.put("username",this.serveurFtp.getIdentifiant());
		header.put("password",this.serveurFtp.getPass());
		header.put("port",this.serveurFtp.getPort());
		String codeRetour = UrlFlopBox.downloadFile(this.serveurFtp.getNomServeurFtp(), file, this.dossierLocal, header);
		if(codeRetour.equals("200")) {
			String[] tabFichier = (this.dossierLocal+file).split("/");
			String nomFichier = tabFichier[tabFichier.length-1];
			String cheminFichier = "./";
			for(int i=0;i<tabFichier.length-1;i++) { cheminFichier += tabFichier[i]+"/"; }
			FichierLocal f = new FichierLocal(nomFichier, cheminFichier);
			String date = UrlFlopBox.getLastModifFile(this.serveurFtp.getNomServeurFtp(), file, header).split("\n")[1];
			String myDate = date.substring(0, 4)+"/"+date.substring(4, 6)+"/"+date.substring(6, 8)+" "+
					date.substring(8, 10)+":"+date.substring(10, 12)+":"+date.substring(12, 14);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			try {
				f.setDateDerniereModification(FileTime.fromMillis(sdf.parse(myDate).getTime()));
				this.mapFichierLocal.put(file, f);
			} catch (FTPServeurException e) {
				// TODO Auto-generated catch block
				System.out.print("error modif date write");
			} catch (ParseException e) {
				System.out.print("error modif date read");
			} 
		}
		else {
			System.out.print("Impossible de télécharger le fichier : "+file+"\n");
		}
	}

	/**
	 * upload un fichier local vers le serveur ftp
	 * @param file un fichier local
	 */
	private void upload(String file) {
		Map<String, String> header = new HashMap<String, String>();
		header.put("username",this.serveurFtp.getIdentifiant());
		header.put("password",this.serveurFtp.getPass());
		header.put("port",this.serveurFtp.getPort());
		
		
		String[] cheminSplit = file.split("/");
		String cheminTmp = "";
		
		
		//Creation dossier precedent si besoin
		for(int i=0;i<cheminSplit.length-1;i++) {
			cheminTmp+=cheminSplit[i]+"/";
			if(UrlFlopBox.getList(this.serveurFtp.getNomServeurFtp(), cheminTmp, header).split("\n").length==1) {
				UrlFlopBox.mkdir(this.serveurFtp.getNomServeurFtp(), cheminTmp, header);
			}
		}

		String codeRetour = UrlFlopBox.uploadFile(this.serveurFtp.getNomServeurFtp(), file, this.dossierLocal, header);

		if(codeRetour.equals("200")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			String dateCreated = sdf.format(this.mapFichierLocal.get(file).getDateDerniereModification().toMillis());
			header.put("date", dateCreated);
			UrlFlopBox.setLastModifFile(this.serveurFtp.getNomServeurFtp(), file, header);
		}
	}
}
