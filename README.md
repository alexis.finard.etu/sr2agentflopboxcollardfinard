# Agent FlopBox COLLARD FINARD

## Introduction

L'objectif du projet est de développer une application cliente pour la plate-forme FlopBox. Celle-ci doit  permettre de synchroniser les données stockées à distance dans un ensemble de serveurs FTP avec le système de fichiers local d'une machine sur laquelle l'application cliente sera exécutée.
#### Compilation et exécution
pour compiler le client,  vous pouvez faire `cd agentFlopBoxCollardFinard/ && mvn package && cd -`   
pour compiler la plate-forme flopbox, vous pouvez faire `cd sr2-projet1-flopbox-FLOPBOX-NGUYEN-modifier/FlopBox/ && mvn package && cd -`   


Pour executer le projet, il faut d'abord executer dans un terminal la plate-forme flopbox, puis le client.   
la commande de la plate-forme : `cd sr2-projet1-flopbox-FLOPBOX-NGUYEN-modifier/FlopBox/ && mvn exec:java && cd - || cd -`   
la commande du client : `cd agentFlopBoxCollardFinard/ && mvn exec:java || cd -`   

Attention : les informations de connexion sont à fournir dans le fichier *agentFlopBoxCollardFinard/LogFile.txt*

#### Exemple d'utilisation
à partir de la racine du dépôt git,
* faites `./scripts/compil.bash` pour compiler les deux projets
* dans un nouveau terminal , faite `./scripts/serveur_local.bash` pour créer un serveur local d'exemple
* dans un nouveau terminal, faite `./scripts/execute_flopbox.bash` pour lancer l'execution du projet flopbox
* dans un nouveau terminal, faite `./scripts/execute_client.bash ` pour lancer l'execution du client flopbox
#### Démonstration video
![Démonstration vidéo](docs/presentation.gif)
## Architecture

Le projet est composé de plusieurs packages. Les packages principaux sont:   

* fichier
* passerelleServeurLocal
* urlFlopBox

Le package *fichier* contient les classes représentant les fichiers, notamment ceux stockés en local côté client ainsi que ceux stockés en distant côté serveur.   
Le package *passerelleServeurLocal* a pour but de faire la passerelle entre fichier distant et fichier locaux.   
Le package *urlFlopBox* s'occupe des envoies d'URL à la plate-forme FlopBox.

#### Gestion d'erreur

Le projet contient un package *exception* représentant une exception liée aux serveurs FTP.   
cette exception est par exemple utilisé pour les méthodes de la classe *Fichier*.
```java
public abstract FileTime getDateDerniereModification() throws FTPServeurException;
```


## Code Samples

#### envoie d'url générique

La classe UrlFlopBox possède une méthode *sendUrl* qui est utilisé dans de nombreux cas, comme par exemple l'ajout d'un Serveur ou encore la récupération de la date de modification d'un fichier.   

```java
public static String addServeur(String nomServeurFtp) {
	return sendUrl(nomServeurFtp, "", "POST", null);
}
...
...
public static String getLastModifFile(String nomServeurFtp, String cheminFichier, Map<String, String> header) {
	return sendUrl(nomServeurFtp, "/file/modif"+cheminFichier , "GET", header);
}
...
...
...
private static String sendUrl(String nomServeurFtp, String action, String typeMsg, Map<String, String> header) {
```

#### le décalage horraire

Lors des comparaisons de date de fichiers, l'un des problèmes rencontrés était le fait que nous n'avions pas les dates attendues.
Le problème était dû aux décalages horaires
```java
SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
```

#### Polling

Afin de rester synchroniser avec les serveurs à distances, une méthode de verification est appelée toutes les 60 secondes.
```java
while(true) {
	System.out.print("En cour de fonctionnement\n"+listPasserelle.size()+" serveurFTP gérer\n");
	for(PasserelleServeurLocal p : listPasserelle) {
		p.check();
	}


	try {
		TimeUnit.MINUTES.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
```

#### comparaisons de fichier

La classe Fichier contient la méthode compDateModif ce qui nous permet de comparer un fichier distant avec un fichier local et vice-versa.
```java
public int compDateModif(Fichier otherFile) throws FTPServeurException {
	return this.getDateDerniereModification().compareTo(otherFile.getDateDerniereModification());
}
```