# SR2-Projet1-FlopBox

Réalisé par Kévin Nguyen

## Introduction

Le but de ce projet est de réalisé une API permettant d'utiliser des commandes curl
pour faire des manipulations avec les serveurs FTP.

## Commentaire

- Les exemples de commandes que l'on peut envoyer au serveur ceux trouvent de le fichier
INSTRUCTION.md

- Toutes les fonctionnalitées ont étaient implémenté sauf pour :

	- la gestion actif passif
	- stockage d'un dossier complet
	- récuperer le contenu complet d'un dossier

## Instruction

Depuis la racine du projet pour générer la javadoc, il faut éxecuter la commande :

mvn javadoc:javadoc

Si jamais la génération de la javadoc échoue, une potentiel solution est de configurer le chemin d'accès a la commande javadoc puis de recommencer la commande pour la javadoc, sur linux :

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

La documentation générée ce trouve dans le dossier target puis le dossier site pis le dossier apidocs, et enfin ouvrir le fichier index-all.html

Depuis la racine du projet pour compiler l'application, il faut éxecuter la commande :

mvn package

Depuis la racine du projet pour éxecuter l'application, il faut éxecuter la commande :

java -jar target/simple-service-1.0-SNAPSHOT-jar-with-dependencies.jar

## Architecture



###Catch :

IOException :

On intercepte une erreur qui est produite par le serveur, on va l'attraper et en relancer une nouvelle
pour donner plus de précision a l'utilisateur sur l'erreur sans faire crasher l'API.

ConnectionServerException :

Lorsque qu'il y a un problème avec la connexion au serveur FTP.
On va envoyer un code d'erreur et informer d'une erreur de technique lors de la connexion au serveur ou accès refusé a l'utilisateur.

FTPServerException :

Lorsque qu'il y a un problème avec le serveur FTP.
On va envoyer un code d'erreur et informer d'une erreur technique avec le serveur a l'utilisateur.

LoginException :

Lorsque que l'on tente de ce connecter avec un identifiant et un mot de passe qui ne fonctionne pas.
On va envoyer un code d'erreur et informer d'une mauvaise combinaison login/password a l'utilisateur.

LogoutException :

Lorsque que l'on tente de ce déconnecter du serveur FTP mais cela ne fonctionne pas.
On va envoyer un code d'erreur et informer d'un problème de déconnexion du serveur FTP a l'utilisateur.

###Throw :

FTPServerException :

Permet de remonter une erreur lors d'une éxecution d'une commande FTP.

LogoutException :

Permet de remonter une erreur de déconnexion du serveur FTP.

LoginException :

Permet de remonter une erreur de login, utilisateur et mot de passe

ConnectionServerException :

Permet de remonter une erreur de connexion au serveur FTP.

## Code Samples

Afin de télécharger un fichier, il faut préparer et configurer une connexion pour récuperer les données qui composent
le fichier.

public void downloadFile(String pathFile) throws FTPServerException {
		try {
			File file = new File(root + pathFile);
			if (!file.exists()) { // we must create all the directories in the path of the file.
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			OutputStream downloaded = new FileOutputStream(file, false);
			this.getClient().setFileType(FTPClient.BINARY_FILE_TYPE); //
			this.getClient().enterLocalPassiveMode();  	          // setup the data connection to gather the data of the file.
			this.getClient().setAutodetectUTF8(true);		   // 
			String filename = changeToFileDirectory(pathFile);
			boolean success = this.getClient().retrieveFile(filename, downloaded);
			if (!success) {
				file.delete();
			}
			downloaded.close();	// close the file once the data has been downloaded
		} catch (IOException e) {
			throw new FTPServerException("Error listing the directory");
		}
	}


Cela permet de supprimer un dossier.

public boolean deleteDirectory(String pathFolder) throws FTPServerException {
		try {
			FTPFile[] files;
			this.getClient().enterLocalPassiveMode();
			files = this.getClient().listFiles(pathFolder); // list all the files contained in the directory
			for (FTPFile file : files) {
				if (file.isDirectory()) {
					deleteDirectory(pathFolder + "/" + file.getName());  // if it is a directory, it redo the same on it
				} else {
					String filePath = pathFolder + "/" + file.getName();
					this.getClient().deleteFile(filePath);
				}
			}
			return this.getClient().removeDirectory(pathFolder); // Once it has deleted all the contents, it delete the directory
		} catch (IOException e) {
			throw new FTPServerException("Error deleting folder.");
		}
	}


Permet de renommer un fichier
@PUT
	@Path("{name}/file/{path: .*}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response renameFile(@PathParam("name") String server, @PathParam("path") String pathFile,
			@HeaderParam("username") String username, @HeaderParam("password") String password,
			@HeaderParam("port") int port, @HeaderParam("rename") String rename) {
		if (!validServer(server)) {
			loadResponse(404, "Erreur serveur non valide.\n"); // check if the server is registered
			return this.response.build();
		}
		port = initialize(port);
		try {
			FtpClient userClient = new FtpClient(listServer.get(server), port, username, password); //
			userClient.connect(); // Create a client, connect, and login to make the client ready for the main instruction to be executed.
			userClient.login();  //
			if (userClient.renameFile(pathFile, rename)) {   // try to rename the file
				loadResponse(200, "Fichier renommé.\n");
			} else {
				loadResponse(404, "Fichier non trouvé\n");
			}
			userClient.logout();	// 		Logout and disconnect once the main instruction is executed.
			userClient.disconnectClient();	//
		} catch (ConnectionServerException e) {
			loadResponse(403, "Erreur connection au serveur FTP refusé.\n");	//
		} catch (FTPServerException e) {
			loadResponse(503, "Erreur problème avec le serveur FTP.\n");		// All we catch any exception raised the execution, send the right error code corresponding.
		} catch (LoginException e) {
			loadResponse(403, "Erreur login ou mot de passe incorrect.\n");		//
		} catch (LogoutException e) {
			loadResponse(503, "Erreur lors de la déconnexion du serveur FTP.\n");	//
		}
		return this.response.build();
	}

Permet de mettre un alias sur un server.

@PUT
	@Path("{name}")
	public void updateServer(@PathParam("name") String server, @HeaderParam("alias") String alias) {
		String serverAssociated = this.listServer.remove(server);  // Remove the old one to attach the alias as the new key
		this.listServer.put(alias, serverAssociated);
	}


Permet de charger une reponse a renvoyer a l'auteur de la requête curl.

public void loadResponse(int status, Object entity) {
		this.response = Response.ok(entity);	// load the response with the object to send back
		this.response.status(status);		// config the response with the status to send back
	}
